/**
 * @description PChallenge Main App 
 * @version 0.0.1
 * @author Christian Ferranti <christian.ferranti@chfstudio.com>
 * @year 2017
 */
var app = angular.module('pchallenge', ['angularModalService','ngAnimate','car','fieldchart','wheelchart','comparisonchart']);
