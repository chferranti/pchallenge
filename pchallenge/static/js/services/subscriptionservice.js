/**
 * @description Subscription Service
 * @version 0.0.1
 * @author Christian Ferranti <christian.ferranti@chfstudio.com>
 * @year 2017
 */

(function () {
    'use strict';

    app.service('subscriptionService', ['$http', '$q', function ($http, $q) {
        this.getSubscriptions = function getSubscriptions() {
            var deferred = $q.defer();
            $http({
                    method: 'GET',
                    url: '/api/subscriptions',
                    cache: true
                })
                .then(function (success){
                    deferred.resolve(success.data);
                },function (error) {
                    deferred.reject('Generic Error in getSubscriptions: '+error);
                });
            return deferred.promise;
        };
    }]);
})();