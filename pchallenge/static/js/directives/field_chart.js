/**
 * @description Field Chart Directive (Sensors in Tyres)
 * @version 0.0.1
 * @author Christian Ferranti <christian.ferranti@chfstudio.com>
 * @year 2017
 */
(function (document, window, angular) {
    'use strict';
    angular.module('fieldchart', []).
    directive('fieldchart', function ($compile, $document,$window) {
        var wheels = [{
                "name": "Front Left",
                "backgroundColor": "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                pointBorderColor: "rgba(75,192,192,1)",
                pointHoverBackgroundColor: "rgba(75,192,192,1)"
            },
            {
                "name": "Front Rigth",
                "backgroundColor": "rgba(55, 123, 232,0.4)",
                borderColor: "rgba(55, 123, 232,1)",
                pointBorderColor: "rgba(55, 123, 232,1)",
                pointHoverBackgroundColor: "rgba(55, 123, 232,1)"
            },
            {
                "name": "Rear Left",
                "backgroundColor": "rgba(190, 232, 53,0.4)",
                borderColor: "rgba(190, 232, 53,1)",
                pointBorderColor: "rgba(190, 232, 53,1)",
                pointHoverBackgroundColor: "rgba(190, 232, 53,1)"
            },
            {
                "name": "Rear Rigth",
                "backgroundColor": "rgba(232, 94, 53,0.4)",
                borderColor: "rgba(232, 94, 53,1)",
                pointBorderColor: "rgba(232, 94, 53,1)",
                pointHoverBackgroundColor: "rgba(232, 94, 53,1)"
            }
        ];

        function initChartdata() {
            var chartData = {
                "labels": [],
                "datasets": []
            };
            wheels.forEach(function (element, index) {
                chartData.datasets.push({
                    label: element.name,
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: element.backgroundColor,
                    borderColor: element.borderColor,
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: element.pointBorderColor,
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [],
                    spanGaps: false
                });
            });
            return chartData;
        }

        function controller($scope) {
            $scope.$on('init',function(event,history_msgs){
                history_msgs.forEach(function(msg){
                    var current_values = msg[$scope.car];
                    $scope.chartData.labels.push(msg.timestamp);
                    $scope.chartData.datasets.forEach(function(dataset,datasets_index){
                        if(current_values!= undefined && current_values[dataset.label] !=null  && current_values[dataset.label][$scope.field]!=null){
                        $scope.chartData.datasets[datasets_index].data.push(current_values[dataset.label][$scope.field]);
                        }
                    });
                    $scope.chart.update();
                });
                history_msgs = null;
            });

            $scope.$on('updateFields', function (event, msg) {
                var current_values = msg[$scope.car];
                $scope.chartData.labels.push(msg.timestamp);
                $scope.chartData.datasets.forEach(function(dataset,datasets_index){
                    if(current_values!= undefined && current_values[dataset.label] !=null  && current_values[dataset.label][$scope.field]!=null){
                       $scope.chartData.datasets[datasets_index].data.push(current_values[dataset.label][$scope.field]);
                    }
                });
                msg = null;
                $scope.chart.update();
            });
        }

        function link($scope, element, attrs) {      
            $scope.chartData = initChartdata();      
            angular.element(document).ready(function () {
                var chart = new Chart(document.getElementById("field_" + $scope.car + "_" + $scope.field).getContext("2d"), {
                    type: 'line',
                    data: $scope.chartData,
                    options: {
                       
                    }
                });
                $scope.chart = chart;
            });
        }

        return {
            restrict: 'E',
            template: function (element, attrs) {
                var width = ($window.innerWidth- 620)/2;
                return '<label>{{label}}</label><canvas class=\'chart\' id=\'field_{{car}}_{{field}}\' width="'+width+'" height="300"><!-- ff--></canvas>';
            },
            scope: {
                label:'@',
                car: '@',
                field: '@'
            },
            replace: false,
            transclude: false,
            link: link,
            controller: controller
        };
    });
})(document, window, window.angular);