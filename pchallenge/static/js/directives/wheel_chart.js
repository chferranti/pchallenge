/**
 * @description Wheel Chart Directive (Sensor detail in Tyre)
 * @version 0.0.1
 * @author Christian Ferranti <christian.ferranti@chfstudio.com>
 * @year 2017
 */
(function (document, window, angular) {
    'use strict';
    angular.module('wheelchart', []).
    directive('wheelchart', function ($compile, $document, $window) {
        var fields = [{
                name: "temperature",
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                pointBorderColor: "rgba(75,192,192,1)",
                pointHoverBackgroundColor: "rgba(75,192,192,1)"
            },
            {
                name: "pressure",
                backgroundColor: "rgba(55, 123, 232,0.4)",
                borderColor: "rgba(55, 123, 232,1)",
                pointBorderColor: "rgba(55, 123, 232,1)",
                pointHoverBackgroundColor: "rgba(55, 123, 232,1)"
            },
            {
                name: "omega",
                backgroundColor: "rgba(190, 232, 53,0.4)",
                borderColor: "rgba(190, 232, 53,1)",
                pointBorderColor: "rgba(190, 232, 53,1)",
                pointHoverBackgroundColor: "rgba(190, 232, 53,1)"
            },
            {
                name: "speed",
                backgroundColor: "rgba(232, 94, 53,0.4)",
                borderColor: "rgba(232, 94, 53,1)",
                pointBorderColor: "rgba(232, 94, 53,1)",
                pointHoverBackgroundColor: "rgba(232, 94, 53,1)"
            }
        ];

        function initChartdata(field) {
            var chartData = {
                "labels": [],
                "datasets": []
            };
            fields.forEach(function (element, index) {
                if (element.name == field) {
                    chartData.datasets.push({
                        label: element.name.charAt(0).toUpperCase() + element.name.slice(1),
                        fill: true,
                        lineTension: 0.1,
                        backgroundColor: element.backgroundColor,
                        borderColor: element.borderColor,
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: element.pointBorderColor,
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [],
                        spanGaps: false
                    });
                }
            });
            return chartData;
        }

        function controller($scope) {
            $scope.$on('updateFields', function (event, msg) {
                var current_values = msg[$scope.car];
                if (current_values!= undefined && current_values[$scope.normalize_position] != null && current_values[$scope.normalize_position][$scope.field] != null) {
                    $scope.chartData.labels.push(msg.timestamp);            
                    $scope.chartData.datasets[0].data.push(current_values[$scope.normalize_position][$scope.field]);
                }
                $scope.chart.update();
            });

            $scope.$watch('position',function(newValue,oldValue){
                var position = angular.copy($scope.position);
                $scope.chartData.datasets[0].data = [];
                $scope.chartData.labels=[];
                $scope.normalize_position = position.indexOf('Right')!=-1 ? position.replace('Right','Rigth') : position;
            });
        }

        function link($scope, element, attrs) {
            $scope.chartData = initChartdata($scope.field);
            angular.element(document).ready(function () {
                var chart = new Chart(document.getElementById("wheel_" + $scope.car+"_"+$scope.field + "_" + $scope.position).getContext("2d"), {
                    type: 'line',
                    data: $scope.chartData,
                    options: {
                        legend: {
                            onClick: function(event, legendItem) {}
                        }
                    }
                });
                $scope.chart = chart;
            });
        }

        return {
            restrict: 'E',
            template: function (element, attrs) {
                var width = ($window.innerWidth - 620) / 2;
                return '<canvas class=\'chart\' id=\'wheel_{{car}}_{{field}}_{{position}}\' width="' + width + '" height="300"><!-- ff--></canvas>';
            },
            scope: {
                label: '@',
                field: '@',
                car:'@',
                position: '@'
            },
            replace: false,
            transclude: false,
            link: link,
            controller: controller
        };
    });
})(document, window, window.angular);