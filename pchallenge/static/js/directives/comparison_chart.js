/**
 * @description Comparison Chart Directive (Compare two fieldss)
 * @version 0.0.1
 * @author Christian Ferranti <christian.ferranti@chfstudio.com>
 * @year 2017
 */
(function (document, window, angular) {
    'use strict';
    angular.module('comparisonchart', []).
    directive('comparisonchart', function ($compile, $document, $window) {
        var fields = [{
                name: "temperature",
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                pointBorderColor: "rgba(75,192,192,1)",
                pointHoverBackgroundColor: "rgba(75,192,192,1)"
            },
            {
                name: "pressure",
                backgroundColor: "rgba(55, 123, 232,0.4)",
                borderColor: "rgba(55, 123, 232,1)",
                pointBorderColor: "rgba(55, 123, 232,1)",
                pointHoverBackgroundColor: "rgba(55, 123, 232,1)"
            },
            {
                name: "omega",
                backgroundColor: "rgba(190, 232, 53,0.4)",
                borderColor: "rgba(190, 232, 53,1)",
                pointBorderColor: "rgba(190, 232, 53,1)",
                pointHoverBackgroundColor: "rgba(190, 232, 53,1)"
            },
            {
                name: "speed",
                backgroundColor: "rgba(232, 94, 53,0.4)",
                borderColor: "rgba(232, 94, 53,1)",
                pointBorderColor: "rgba(232, 94, 53,1)",
                pointHoverBackgroundColor: "rgba(232, 94, 53,1)"
            }
        ];

        function initChartdata(field) {
            var chartData = {
                "labels": [],
                "datasets": []
            };
            fields.forEach(function (element, index) {
                if (element.name == field) {
                    chartData.datasets.push({
                        label: element.name.charAt(0).toUpperCase() + element.name.slice(1),
                        fill: true,
                        lineTension: 0.1,
                        backgroundColor: element.backgroundColor,
                        borderColor: element.borderColor,
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: element.pointBorderColor,
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [],
                        spanGaps: false
                    });
                }
            });
            return chartData;
        }

        function controller($scope) {
            $scope.$on('updateFields', function (event, msg) {
                $scope.members.forEach(function(member,index){
                    var current_values = msg[member.name];
                    if (current_values!= undefined && current_values[member.position] != null && current_values[member.position][member.field] != null) {
                        $scope.charts[index].data.labels.push(msg.timestamp);            
                        $scope.charts[index].data.datasets[0].data.push(current_values[member.position][member.field]);
                    }
                    $scope.charts[index].update();
                });
                
            });
        }

        function link($scope, element, attrs) {
            $scope.width = ($window.innerWidth - 80) / 2;
            $scope.charts = [];
            for (var item in sessionStorage) {
                if (item.indexOf($scope.name) != -1 || item.indexOf("comparison_" + $scope.name)) {
                    var comparison = JSON.parse(sessionStorage.getItem(item));
                    $scope.members = comparison.members;
                }
            }

            angular.element(document).ready(function () {
                $scope.members.forEach(function (member, index) {
                    member.position = member.position.indexOf('Right')!=-1 ? member.position.replace('Right','Rigth') : member.position;
                    member.field = member.field.toLowerCase();
                    index++;
                    var chart = new Chart(document.getElementById("comparison_" + $scope.name + "_" + index).getContext("2d"), {
                        type: 'line',
                        data: initChartdata(member.field),
                        options: {
                            legend: {
                                onClick: function (event, legendItem) {}
                            }
                        }
                    });
                    $scope.charts.push(chart);
                });
            });

        }

        return {
            restrict: 'E',
            templateUrl: function (element, attrs) {
                return '/static/html/templates/comparison_chart.html';
            },
            scope: {
                name: '@'
            },
            replace: false,
            transclude: false,
            link: link,
            controller: controller
        };
    });
})(document, window, window.angular);