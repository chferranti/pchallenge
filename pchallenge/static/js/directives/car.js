/**
 * @description Car Directive
 * @version 0.0.1
 * @author Christian Ferranti <christian.ferranti@chfstudio.com>
 * @year 2017
 */
(function (document, window, angular) {
    'use strict';
    angular.module('car', []).
    directive('car', function ($compile, $document) {

        var positions = [{
                'name': 'Front Left',
                left: 25,
                top: 55
            },
            {
                'name': 'Front Right',
                left: 295,
                top: 55
            },
            {
                'name': 'Rear Right',
                left: 295,
                top: 282
            },
            {
                'name': 'Rear Left',
                left: 25,
                top: 282
            }
        ];

        var fields = [{
                'field': 'temperature',
                'defaultText': 'C°: '
            },
            {
                'field': 'pressure',
                'defaultText': 'bar: '
            },
            {
                'field': 'omega',
                'defaultText': unescape('%u03C9') + ': '
            },
            {
                'field': 'speed',
                'defaultText': 'km/h: '
            }
        ];

        function buildTimestamp(canvas) {
            var text = new fabric.Text('', {
                id: 'timestamp',
                fontSize: 18,
                left: 125,
                top: 10
            });
            text.set({
                selectable: false
            });
            text.setColor('white');
            canvas.add(text);
            return canvas;
        }

        function buildRect(position) {
            return new fabric.Rect({
                id: position.name,
                selected: false,
                left: position.left > 150 ? position.left - 35 : position.left - 5,
                top: position.top,
                originX: 'left',
                originY: 'top',
                width: 120,
                height: 85,
                angle: 0,
                fill: 'rgba(255,255,255,0)',
                selectable: false,
                hoverCursor: 'pointer',
                transparentCorners: false
            });
        }

        function buildPositions(canvas) {
            positions.forEach(function (index) {
                var text = new fabric.Text(index.name, {
                    fontSize: 18,
                    left: index.left,
                    top: index.top
                });
                text.set({
                    selectable: false
                });
                text.setColor('white');
                canvas.add(text);
                var spacer = 15;
                fields.forEach(function (field) {
                    var left = index.left - 5;
                    var top = index.top + 5;
                    var fieldtext = new fabric.Text(field.defaultText, {
                        field: field.field,
                        position: index.name.replace('Right', 'Rigth'),
                        defaultText: field.defaultText,
                        fontSize: 12,
                        left: left,
                        top: top + spacer
                    });
                    fieldtext.set({
                        selectable: false,
                    });
                    fieldtext.setColor('white');
                    canvas.add(fieldtext);
                    spacer += 15;
                });
                canvas.add(buildRect(index));
            });
            return canvas;
        }

        function controller($scope) {
            $scope.$on('updateFields', function (event, msg) {
                var current_values = msg[$scope.name];
                if (current_values != null) {
                    $scope.canvas.forEachObject(function (o) {
                        if (o.isType('text')) {
                            if (o.id == 'timestamp') {
                                o.set({
                                    text: msg.timestamp
                                });
                            } else if (o.field != null && o.position != null && current_values[o.position] != null) {
                                o.set({
                                    text: o.defaultText + Number(current_values[o.position][o.field].toFixed(2))
                                });
                            }
                        }
                    });
                    $scope.canvas.renderAll();
                }
            });
        }

        function link($scope, element, attrs) {

            $scope.$watch('canvasid', function () {
                var canvas = new fabric.Canvas($('.car_' + $scope.canvasid)[0]);
                var backgroup_image = new Image();
                backgroup_image.src = '/static/images/car_chassis.png';
                backgroup_image.onload = function () {
                    canvas.setBackgroundImage(backgroup_image.src, canvas.renderAll.bind(canvas), {
                        originX: 'left',
                        originY: 'top',
                        left: 113,
                        top: 30
                    });
                }

                canvas = buildPositions(canvas);
                canvas = buildTimestamp(canvas);
                canvas.renderAll();

                canvas.on('mouse:over', function (e) {
                    if (e.target != null && e.target.type == 'rect') {
                        e.target.set({
                            fill: 'rgba(187,188,190,0.5)'
                        });
                    }
                    canvas.renderAll();
                });

                
                canvas.on('mouse:out', function (e) {
                    canvas.forEachObject(function (o) {
                        if (o.isType('rect') && o.selected==false) {
                            o.set({
                                fill: 'rgba(255,255,255,0)'
                            });
                        }
                    });
                    canvas.renderAll();
                });

                canvas.on('mouse:down', function (e) {
                    if (e.target != null && e.target.type == 'rect') {
                        if (e.target.selected) {
                            e.target.selected = false;
                            e.target.set({
                                fill: 'rgba(255,255,255,0)'
                            });
                            $scope.$emit('removePositionDetails', {
                                'position': e.target.id,
                                'car': $scope.name
                            });
                        } else {
                            e.target.set({
                                fill: 'rgba(187,188,190,0.5)'
                            });
                            e.target.selected = true;
                            canvas.forEachObject(function (o) {
                                if (o.isType('rect') && o.id != e.target.id) {
                                    o.set({
                                        fill: 'rgba(255,255,255,0)'
                                    });
                                    o.selected = false;
                                }
                            });
                            $scope.$emit('getPositionDetails', {
                                'position': e.target.id,
                                'car': $scope.name
                            });
                        }
                    }
                });

                $scope.canvas = canvas;
            });
        }

        return {
            restrict: 'E',
            template: function (element, attrs) {
                return '<canvas class=\'car_{{canvasid}}\' width=\'400\' height=\'425\'><!-- ff--></canvas>'
            },
            scope: {
                name: '@',
                canvasid: '@'
            },
            replace: false,
            transclude: false,
            link: link,
            controller: controller
        };
    });
})(document, window, window.angular);