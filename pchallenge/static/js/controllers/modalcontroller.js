/**
 * @description PChallenge Modal Controller
 * @version 0.0.1
 * @author Christian Ferranti <christian.ferranti@chfstudio.com>
 * @year 2017
 */

(function () {
    'use strict';

    function ModalController($scope, close) {
        $scope.fields = ["Temperature", "Pressure", "Speed", "Omega"];
        $scope.positions = ["Front Right", "Front Left", "Rear Right", "Rear Left"];
        $scope.comparisons_names = [];
        
        // Saved Comparison retrieving to avoid name duplicates
        for(var item in sessionStorage){
            if(item.indexOf("comparison_")!=-1){
                var comparison = JSON.parse(sessionStorage.getItem(item))
                $scope.comparisons_names.push(comparison.name);
            }
        }

        $scope.name = null;
        $scope.first_member = {
            name: null,
            position: null,
            field: null
        };
        $scope.second_member = {
            name: null,
            position: null,
            field: null
        };

        $scope.comparison = {};
        $scope.cars = JSON.parse(sessionStorage.getItem("cars"));

        $scope.onClose = function onClose() {
            close($scope.comparison, 250);
        };

        $scope.isCompleted = function isCompleted() {
            if ($scope.name == null || $scope.comparisons_names.indexOf($scope.name)!=-1 || 
                checkNull($scope.first_member) || checkNull($scope.second_member)) {
                return false;
            }
            $scope.comparison = {name:angular.copy($scope.name),members:[]};
            $scope.comparison.members.push(angular.copy($scope.first_member));
            $scope.comparison.members.push(angular.copy($scope.second_member));
            //$scope.comparisons_names.push($scope.comparison.name);
            return true;
        };
    }

    function checkNull(obj) {
        for (var key in obj) {
            if (obj[key] == null)
                return true;
        }
        return false;
    }

    var app = angular.module('pchallenge').controller('ModalController', ModalController);
})();