/**
 * @description PChallenge Main Controller
 * @version 0.0.1
 * @author Christian Ferranti <christian.ferranti@chfstudio.com>
 * @year 2017
 */

(function () {
    'use strict';

    function MainController($scope, $window, subscriptionService, ModalService) {
        Chart.defaults.global.responsive = false;

        $scope.cars = [];
        $scope.comparisons = [];


        // Retrieve all saved Comparisons
        for(var item in sessionStorage){
            if(item.indexOf("comparison_")!=-1){
                $scope.comparisons.push(JSON.parse(sessionStorage.getItem(item)));
            }
            $scope.comparisons = $scope.comparisons.reverse();
        }
        

        // Retrieve all cars 
        var subscriptions = subscriptionService.getSubscriptions();
        subscriptions.then(function (data) {
            var results = data.subscriptions;
            if (results.length > 0) {
                results.forEach(function (index) {
                    $scope.cars.push({
                        'name': index.name,
                        visible: true,
                        wheel_detail: false
                    });
                });
                sessionStorage.setItem("cars",JSON.stringify($scope.cars));
                $scope.startStreaming();
            }
        }, function (reason) {
            console.log(reason);
        });

        // Simple streaming
        $scope.startStreaming = function startStreaming() {
            var socket = io.connect('http://' + document.domain + ':' + location.port + '/pchallenge');
            socket.on('all fields', function (msg) {
                $scope.$broadcast('updateFields', msg);
                msg = null;
            });

            socket.on('init', function (history_msgs) {
                $scope.$broadcast('init', history_msgs);
                history_msgs = null;
            });
            console.log("Start Streaming...");
            socket.emit('startStreaming', {});
        };

        // Select Wheel from Car Directive
        $scope.$on('getPositionDetails', function (event, msg) {
            $scope.cars.forEach(function (index) {
                if (index.name == msg.car) {
                    index.wheel_detail = true;
                    index.selected_position = msg.position;
                }
            });
            sessionStorage.setItem("cars",JSON.stringify($scope.cars));
            $scope.$apply();
        });

        // Deselect Wheel from Car Directive
        $scope.$on('removePositionDetails', function (event, msg) {
            $scope.cars.forEach(function (index) {
                index.wheel_detail = false;
            });
            sessionStorage.setItem("cars",JSON.stringify($scope.cars));
            $scope.$apply();
        });

        // At least one car must be selected
        $scope.$watch('cars', function (newValue, oldValue) {
            var all_invisible = [];
            newValue.forEach(function (index) {
                all_invisible.push(index.visible);
            });
            if (all_invisible.indexOf(true) == -1) {
                newValue.forEach(function (index) {
                    index.visible = true;
                });
            }
            sessionStorage.setItem("cars",JSON.stringify($scope.cars));
        }, true);


        $scope.showComparisonModal = function showComparisonModal() {
            ModalService.showModal({
                templateUrl: "/static/html/templates/modals/comparison_modal.html",
                controller: "ModalController"
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    console.log("Adding New Comparison");
                    sessionStorage.setItem("comparison_"+result.name,JSON.stringify(result));
                    $scope.comparisons.push(result);
                    $scope.comparisons = $scope.comparisons.reverse();
                    $scope.$broadcast("newComparison",result);
                });
            });
        };

        $scope.removeComparison = function removeComparison(index){
            var comparison = $scope.comparisons[index];
            var cleaned_comparisons = [];
            sessionStorage.removeItem("comparison_"+comparison.name);
            $scope.comparisons.forEach(function(index){
                if(index.name != comparison.name){
                    cleaned_comparisons.push(index);   
                }
            });
            $scope.comparisons = cleaned_comparisons;
        };


    };
    var app = angular.module('pchallenge').controller('MainController', MainController);
})();
