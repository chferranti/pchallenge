import csv 
import threading
import ConfigParser
from pymongo import MongoClient
from time import sleep
import datetime
from flask import Flask, render_template,redirect, request, url_for,jsonify
from flask_socketio import SocketIO, emit

app = Flask(__name__)
app.config['PROPAGATE_EXCEPTIONS'] = True
socketio = SocketIO(app,async_mode=None)

config = ConfigParser.ConfigParser()
config.readfp(open('conf/pchallenge.conf'))

mongo_client = MongoClient(config.get('MongoDB','uri'),serverSelectionTimeoutMS = 60*1000)
mongo_database = mongo_client.get_database(config.get('MongoDB','db'))
sensors_collection = mongo_database.get_collection (config.get('MongoDB','sensors_collection'))
subscribed_collection =  mongo_database.get_collection (config.get('MongoDB','subscribed_collection'))

port = config.get('General','port')
history = []


def getMinutes(firstDate,secondDate):
   return (firstDate-secondDate).total_seconds() / 60.0

def getInitialTimestamp():
    pipeline = [{'$group': {'_id': None, 'timestamp': {'$min': '$timestamp'}}}]
    cursor = sensors_collection.aggregate(pipeline=pipeline)
    return list(cursor)[0]['timestamp']   

def getSubscriptions():
    cursor = subscribed_collection.find({'name':{'$exists':True}},{'_id':0,'insertDate':0})
    subscriptions = []
    for subscribed in cursor:
        subscriptions.append(subscribed)
    return subscriptions 

def getAllFields():
    with app.app_context():
        initial_timestamp = getInitialTimestamp()
        current_timestamp = initial_timestamp
        check_timestamp = initial_timestamp
        while True:
            data = sensors_collection.find_one({'timestamp':current_timestamp})
            del data['_id']
            data['timestamp'] = data['timestamp'].strftime('%Y-%m-%d %H:%M')
            # Insert into history every 10 lectures (only for demo reasons)
            if getMinutes(current_timestamp,check_timestamp) >= 1:
                history.append(data)
                check_timestamp = current_timestamp
            socketio.emit('all fields',data,namespace='/pchallenge')
            current_timestamp = current_timestamp + datetime.timedelta(minutes=1)
            sleep(1)

@socketio.on('startStreaming', namespace='/pchallenge')
def startStreaming(message):
    if len(history) > 0 :
        print 'Streaming yet activated!'
    else:
        t = threading.Thread(target=getAllFields)
        t.daemon = True
        t.start()
        print 'Activated streaming!'

@socketio.on('connect', namespace='/pchallenge')
def connect():
    emit('init',history)

@socketio.on('disconnect', namespace='/pchallenge')
def disconnect():
    print('Client disconnected')

@app.route('/')
def index():
    #return redirect(url_for('static', filename='html/index.html'))
    return render_template('index.html')

@app.route('/api/subscriptions', methods=['GET'])
def get_tasks():
    return jsonify({'subscriptions': getSubscriptions()})


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0',port=port)