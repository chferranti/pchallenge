import ConfigParser
import csv
import datetime
import logging
import os
import threading
from pymongo import MongoClient


def insertSplitted(logger,mongo_collection,splitted_file):
    logger.info("Insert into MongoDb Collection "+splitted_file)
    csv_file = csv.DictReader(open(splitted_file,"rU"),delimiter=',')
    for row in csv_file:
        obj = {}
        obj['timestamp'] = datetime.datetime.strptime(row['Timestamp'], '%Y-%m-%d %H:%M:%S')
        position = {}
        sensors = {}
        sensors['temperature'] = float(row['Temp']) if row['Temp'] else None
        sensors['speed'] = float(row['Speed']) if row['Speed'] else None
        sensors['pressure'] = float(row['Press']) if row['Press'] else None
        if sensors['pressure'] < 0:
            sensors['pressure'] = 0
        sensors['omega'] = float(row['Omega'])  if row['Omega'] else None
        if sensors['omega'] < 0:
            sensors['omega'] = 0
        position[row['Position']] = sensors
        obj[row['Car_id']] = position
        mongo_collection.update({"timestamp":obj['timestamp']},{'$set':{row['Car_id']+'.'+row['Position']:sensors}},upsert=True)
            
def main(args=None):

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)
    
    logger.info("[START] Initial Load")

    config = ConfigParser.ConfigParser()
    config.readfp(open('conf/initial_load.conf'))

    # Init MongoClient
    logger.info("Establishing MongoDB Connection")
    mongo_client = MongoClient(config.get('MongoDB','uri'),serverSelectionTimeoutMS = 60*1000)
    mongo_database = mongo_client.get_database(config.get('MongoDB','db'))
    subscribed_collection = mongo_database.get_collection (config.get('MongoDB','subscribed_collection'))
    sensors_collection = mongo_database.get_collection (config.get('MongoDB','sensors_collection'))

    # Empty Collections in Initial Load
    logger.info("Remove old data in collections")
    sensors_collection.remove(multi=True)
    subscribed_collection.remove(multi=True)
    
    #Create Index 
    if 'timestamp' not in sensors_collection.index_information():
        sensors_collection.create_index('timestamp')
     # Init raw csv_file
    logger.info("Reading csv raw messages file")
    raw_file_path = config.get('Raw-Data','path')+config.get('Raw-Data','csv_file')
    csv_file = csv.DictReader(open(raw_file_path,"rU"),delimiter=',')

    
    # Create Splitted Data Files
    splitted_files = []
    subscriptions = []
    for row in csv_file:
        current_splitted_file = config.get('Raw-Data','path')+row['Car_id'].replace(" ","_")+".csv"
        with open(current_splitted_file, 'a') as f: 
            w = csv.DictWriter(f, row.keys())
            if current_splitted_file not in splitted_files:
                w.writeheader()
                subscriptions.append(row['Car_id'])
                logger.info("Creating Splitted file"+current_splitted_file)
                splitted_files.append(current_splitted_file)
            w.writerow(row)
    
    for subscribed in subscriptions:
        subscribed_collection.insert({'name':subscribed,'insertDate':datetime.datetime.now()})


    thread_list = []
    
    logger.info("Insert into MongoDb Collections")
    for splitted_file in splitted_files:
        t = threading.Thread(target=insertSplitted,args=(logger,sensors_collection,splitted_file))
        t.daemon = True
        thread_list.append(t)
    
    for t in thread_list:
        strptime = datetime.datetime.strptime #threading bug in strptime
        t.start()

    for t in thread_list:
        t.join()
    
    logger.info("[END] Initial Load")

if __name__ == '__main__':
    main()    

