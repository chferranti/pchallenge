# README #

# Chf PChallenge
This repo contains the project developed for the Technical Challenge called PChallenge.

Contains:

* *pchallenge* - Flask/Angular powered Frontend 

* *initial_load* - MongoDB preparation tool and Initial Load

## Initial Load

Its main duty is to deal with the "data-measurement" csv file and:

* to split it into different csv files arranged by car_id

* to group measurements by timestamp and insert into a MongoDB collection

To run it, place the raw csv file in the 'raw' directory and edit the 'conf/initial_load.conf' with the correct mongodb uri.

## PChallenge

For a quick startup, I have just created a MongoDb database (using the MongoDB Atlas platform), added a read-only and initialized with the Initial Load data.
The default port is 80.